<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'user_id')->dropDownList(User::getList(), ['prompt'=> '--select--']) ?>
        </div>
        <div class="col-md-4">
            <?php echo $form
                ->field($model, 'date_begin_text')
                ->widget('trntv\yii\datetime\DateTimeWidget', ['clientOptions' => [
                    'allowInputToggle' => true,
                    'locale' => 'ru',
                ]]); ?>
        </div>
        <div class="col-md-4">
            <?php echo $form
                ->field($model, 'date_end_text')
                ->widget('trntv\yii\datetime\DateTimeWidget', ['clientOptions' => [
                    'allowInputToggle' => true,
                    'locale' => 'ru',
                ]]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
