<?php

use yii\db\Migration;

/**
 * Class m180525_204415_projects
 */
class m180525_204415_projects extends Migration
{
    public $projects = "{{%projects}}";
    public $users = "{{%users}}";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->projects, [
            'project_id' => $this->primaryKey(),

            'title'      => $this->string(512)->notNull(),
            'price_cost' => $this->bigInteger()->defaultValue(0),
            'date_begin' => $this->integer(),
            'date_end'   => $this->integer(),

            'user_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_projects_user_id', $this->projects, 'user_id');
        $this->addForeignKey(
            'fk_projects_user_id',
            $this->projects,
            'user_id',
            $this->users,
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_projects_user_id', $this->projects);
        $this->dropTable($this->projects);
    }
}
